/*
 * Copyright 2013. Katsumi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.katsumi.resources;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * 複数ファイルのアップロードサンプル
 * @author Katsumi
 * @since 2013/12/21
 */
@Path("/upload")
public class Upload
{
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/multipleFiles")
    public String upload(@FormDataParam("file")List<FormDataBodyPart> bodyParts) throws IOException
    {
        for (FormDataBodyPart part : bodyParts) {
            Files.copy(part.getValueAs(InputStream.class),
                    Paths.get("/Users/Katsumi/Downloads", part.getFormDataContentDisposition().getFileName()));
        }
        return "finish!!";
    }
}
